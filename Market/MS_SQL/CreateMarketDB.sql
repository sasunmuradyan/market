USE [test]
GO

CREATE TABLE PGroups (
    id INT PRIMARY KEY IDENTITY (1, 1),
    group_name NVARCHAR (50) NOT NULL,
    description NVARCHAR (250)
);
GO

CREATE TABLE Products (
    id INT PRIMARY KEY IDENTITY (1, 1),
    product_name NVARCHAR (50) NOT NULL,
	price MONEY NOT NULL,
    description NVARCHAR (250),
    pgroup_id INT NOT NULL,
    FOREIGN KEY (pgroup_id) REFERENCES PGroups (id)
);
GO

CREATE TABLE Contacts (
    id INT PRIMARY KEY IDENTITY (1, 1),
    first_name NVARCHAR (50) NOT NULL,
	last_name NVARCHAR (50) NOT NULL,
	phone VARCHAR(20),
	email VARCHAR(50)
);
GO

CREATE TABLE Sales (
    id INT PRIMARY KEY IDENTITY (1, 1), 
    product_id INT NOT NULL, FOREIGN KEY (product_id) REFERENCES Products (id),
	contact_id INT NOT NULL, FOREIGN KEY (contact_id) REFERENCES Contacts (id),
	product_count INT NOT NULL,
	price MONEY NOT NULL,
    sale_date DATETIME
);
GO

ALTER TABLE Sales
	ADD CONSTRAINT [Sale_Date] DEFAULT(getdate()) FOR sale_date
GO




