﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Market.Classes
{
	public class PGroup
	{
		public int id { get; set; }
		public string groupName { get; set; }
		public string description { get; set; }

		public PGroup()
		{

		}
		public PGroup(int id, string groupName, string description)
		{
			this.id = id;
			this.groupName = groupName;
			this.description = description;
		}

		public int Add()
		{
			int rowAddedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "INSERT INTO PGroups(group_name,[description]) VALUES('" + this.groupName + "', '" + this.description + "'); ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowAddedCount = cmd.ExecuteNonQuery();
			}
			return rowAddedCount;
		}

		public int Update()
		{
			int rowUpdatedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "UPDATE PGroups SET group_name = '" + this.groupName + "', [description] = '" + this.description + "' WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowUpdatedCount = cmd.ExecuteNonQuery();
			}
			return rowUpdatedCount;
		}
		
		public int DeleteById()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM PGroups WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public int DeleteByName()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM PGroups WHERE group_name = " + this.groupName + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public DataTable GetDataTable()
		{
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM PGroups;";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "PGroups");
				return ds.Tables[0];				
			}
		}

		public PGroup GetPGroupsById(int id)
		{
			PGroup pGroups = new PGroup();
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM PGroups WHERE id = " + id + ";";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "PGroups");
				pGroups.id = id;
				pGroups.groupName = ds.Tables[0].Rows[0].Field<string>(1);
				pGroups.description = ds.Tables[0].Rows[0].Field<string>(2);
				return pGroups;
			}
		}
	}
}
