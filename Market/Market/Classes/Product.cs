﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market
{
	public class Product
	{
		public int id { get; set; }
		public string productName { get; set; }
		public decimal price { get; set; }
		public string description { get; set; }
		public int pgroupId { get; set; }

		public Product()
		{

		}
		public Product(int id, string product_name, decimal price, string description, int pgroup_id)
		{
			this.id = id;
			this.productName = productName;
			this.price = price;
			this.description = description;
			this.pgroupId = pgroupId;
		}
		public int Add()
		{
			int rowAddedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "INSERT INTO Products(product_name,price,[description],pgroup_id) VALUES('" + this.productName + "','" + this.price + "','" + this.description + "'," + this.pgroupId + ");";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowAddedCount = cmd.ExecuteNonQuery();
			}
			return rowAddedCount;
		}

		public int Update()
		{
			int rowUpdatedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "UPDATE Products SET product_name = '" + this.productName + "',price = '" + this.price + "',[description] = '" + this.description + "',pgroup_id = " + this.pgroupId + " WHERE id = " + this.id + ";";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowUpdatedCount = cmd.ExecuteNonQuery();
			}
			return rowUpdatedCount;
		}

		public int DeleteById()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM Products WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public DataTable GetDataTable()
		{
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM Products;";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Products");
				return ds.Tables[0];
			}
		}
		public Product GetProductsById(int id)
		{
			Product products = new Product();
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM Products WHERE id = " + id + ";";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Products");
				products.id = id;
				products.productName = ds.Tables[0].Rows[0].Field<string>(1);

				
				products.price =ds.Tables[0].Rows[0].Field<decimal>("price");
				products.description = ds.Tables[0].Rows[0].Field<string>(3);
				products.pgroupId = ds.Tables[0].Rows[0].Field<int>(4);				
				return products;
			}
		}
	}
}
