﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Classes
{
	public class Sale
	{
		public int id { get; set; }
		public int productId { get; set; }
		public int contactId { get; set; }
		public int productCount { get; set; }
		public decimal price { get; set; }
		public DateTime saleDate { get; set; }

		public Sale()
		{

		}
		public Sale(int id, int productId, int contactId, int productCount, decimal price, DateTime saleDate)
		{
			this.id = id;
			this.productId = productId;
			this.contactId = contactId;
			this.productCount = productCount;
			this.price = price;
			this.saleDate = saleDate;
		}
		public int Add()
		{
			int rowAddedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "INSERT INTO Sales (product_id, contact_id, product_count, price, sale_date) VALUES (" + this.productId + ", " + this.contactId + ", " + this.productCount + ", " + this.price + ", " + (DateTime)this.saleDate + ");";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowAddedCount = cmd.ExecuteNonQuery();
			}
			return rowAddedCount;
		}
		public int Update()
		{
			int rowUpdatedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "UPDATE Sales SET product_id = " + this.productId + ", contact_id = " + this.contactId + ", product_count = " + this.productCount + ", price = " + this.price + ", sale_date = " + this.saleDate + " WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowUpdatedCount = cmd.ExecuteNonQuery();
			}
			return rowUpdatedCount;
		}
		public int DeleteById()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM Sales WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public int DeleteByName()
		{
			throw new NotImplementedException();
		}
		public DataTable GetDataTable()
		{
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM Sales;";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Sales");
				return ds.Tables[0];
			}
		}
		public Sale GetSalesById(int id)
		{
			Sale sales = new Sale();
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM Sales WHERE id = " + id + ";";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Sales");
				sales.id = id;
				sales.productId = ds.Tables[0].Rows[0].Field<int>(1);
				sales.contactId = ds.Tables[0].Rows[0].Field<int>(2);
				sales.productCount = ds.Tables[0].Rows[0].Field<int>(3);
				sales.price = ds.Tables[0].Rows[0].Field<int>(4);
				sales.saleDate = ds.Tables[0].Rows[0].Field<DateTime>(4);
				return sales;
				
			}
		}
	}
}
