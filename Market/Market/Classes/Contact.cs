﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.Classes
{
	public class Contact
	{
		public int id { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string phone { get; set; }
		public string email { get; set; }

		public Contact()
		{

		}
		public Contact(int id, string firstName, string lastName, string phone, string email)
		{
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.phone = phone;
			this.email = email;
		}
		public int Add()
		{
			int rowAddedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "INSERT INTO Contacts(first_name, last_name, phone, email) VALUES('" + this.firstName + "', '" + this.lastName + "', '" + this.phone + "', '" + this.email + "'); ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowAddedCount = cmd.ExecuteNonQuery();
			}
			return rowAddedCount;
		}

		public int Update()
		{
			int rowUpdatedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "UPDATE Contacts SET first_name = '" + this.firstName + "', last_name = '" + this.lastName + "', phone = '" + this.phone + "', email = '" + this.email + "' WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowUpdatedCount = cmd.ExecuteNonQuery();
			}
			return rowUpdatedCount;
		}
		public int DeleteById()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM Contacts WHERE id = " + this.id + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public int DeleteByName()
		{
			int rowDeletedCount = 0;
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "DELETE FROM Contacts WHERE group_name = " + this.firstName + "; ";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				SqlCommand cmd = new SqlCommand(query, connection);
				rowDeletedCount = cmd.ExecuteNonQuery();
			}
			return rowDeletedCount;
		}
		public DataTable GetDataTable()
		{
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT *, first_name + ' ' + last_name AS full_name FROM Contacts;";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Contacts");
				return ds.Tables[0];
			}
		}
		public Contact GetContactsById(int id)
		{
			Contact contacts = new Contact();
			using (SqlConnection connection = new SqlConnection(@"Server=localhost; Database=test; Trusted_Connection=True;"))
			{
				connection.Open();
				string query = "SELECT * FROM Contacts WHERE id = " + id + ";";
				SqlDataAdapter da = new SqlDataAdapter(query, connection);
				DataSet ds = new DataSet();
				da.Fill(ds, "Contacts");
				contacts.id = id;
				contacts.firstName = ds.Tables[0].Rows[0].Field<string>(1);
				contacts.lastName = ds.Tables[0].Rows[0].Field<string>(2);
				contacts.phone = ds.Tables[0].Rows[0].Field<string>(3);
				contacts.email = ds.Tables[0].Rows[0].Field<string>(4);
				return contacts;
			}
		}
	}
}
