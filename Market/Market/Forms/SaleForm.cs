﻿using Market.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Market
{
    public partial class SaleForm : Form
    {
        Sale _sale = new Sale();
        string _qType = "";
        public SaleForm()
        {
            InitializeComponent();
        }

        public SaleForm(string qType)
        {
            InitializeComponent();
            _qType = qType;
        }

        private void SaleForm_Load(object sender, EventArgs e)
        {
            Product product = new Product();
            DataTable dt = product.GetDataTable();
            comboProductId.ValueMember = "id";
            comboProductId.DisplayMember = "product_name";
            comboProductId.DataSource = dt;

            Contact contact = new Contact();
            dt = contact.GetDataTable();
            comboContactId.ValueMember = "id";
            comboContactId.DisplayMember = "full_name";
            comboContactId.DataSource = dt;

            switch (_qType)
            {
                case "Insert":
                    button1.Text = "Ավելացնել";
                    break;
                case "Update":
                    button1.Text = "Խմբագրել";
                    break;
                case "Delete":
                    button1.Text = "Ջնջել";
                    break;
                default:
                    break;
            }
            dataGridSale.DataSource = _sale.GetDataTable();
        }

        private void dataGridSale_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((_qType == "Update") || (_qType == "Delete"))
            {
                Sale sale = new Sale();
                sale = sale.GetSalesById((int)dataGridSale.Rows[e.RowIndex].Cells[0].Value);
                comboProductId.SelectedValue = sale.productId;
                comboContactId.SelectedValue = sale.contactId;
                numProductCount.Value = sale.productCount;
                txtPrice.Text = sale.price.ToString();
                dateTimeSale.Value = sale.saleDate;
                _sale = sale;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _sale.productId = (int)comboProductId.SelectedValue;
            _sale.contactId = (int)comboContactId.SelectedValue;
            _sale.productCount = (int)numProductCount.Value;
            _sale.price = decimal.Parse(txtPrice.Text);
            _sale.saleDate = dateTimeSale.Value;

            switch (_qType)
            {
                case "Insert":
                    _sale.Add();
                    dataGridSale.DataSource = _sale.GetDataTable();
                    break;
                case "Update":
                    _sale.Update();
                    dataGridSale.DataSource = _sale.GetDataTable();
                    break;
                case "Delete":
                    _sale.DeleteById();
                    dataGridSale.DataSource = _sale.GetDataTable();
                    break;
                default:
                    break;
            }
        }
    }
}
