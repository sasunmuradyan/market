﻿namespace Market
{
    partial class SaleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboProductId = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboContactId = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numProductCount = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimeSale = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridSale = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numProductCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSale)).BeginInit();
            this.SuspendLayout();
            // 
            // comboProductId
            // 
            this.comboProductId.FormattingEnabled = true;
            this.comboProductId.Location = new System.Drawing.Point(151, 26);
            this.comboProductId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboProductId.Name = "comboProductId";
            this.comboProductId.Size = new System.Drawing.Size(191, 24);
            this.comboProductId.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ապրանք";
            // 
            // comboContactId
            // 
            this.comboContactId.FormattingEnabled = true;
            this.comboContactId.Location = new System.Drawing.Point(151, 63);
            this.comboContactId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboContactId.Name = "comboContactId";
            this.comboContactId.Size = new System.Drawing.Size(191, 24);
            this.comboContactId.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 63);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Գնորդ";
            // 
            // numProductCount
            // 
            this.numProductCount.Location = new System.Drawing.Point(151, 100);
            this.numProductCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numProductCount.Name = "numProductCount";
            this.numProductCount.Size = new System.Drawing.Size(191, 22);
            this.numProductCount.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Ապրանքի քանակ";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(151, 134);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(191, 22);
            this.txtPrice.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Ընդ․ գին";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 174);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Վաճառքի ամսաթիվ";
            // 
            // dateTimeSale
            // 
            this.dateTimeSale.Location = new System.Drawing.Point(151, 169);
            this.dateTimeSale.Name = "dateTimeSale";
            this.dateTimeSale.Size = new System.Drawing.Size(191, 22);
            this.dateTimeSale.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(151, 208);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 36);
            this.button1.TabIndex = 18;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridSale
            // 
            this.dataGridSale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridSale.Location = new System.Drawing.Point(360, 26);
            this.dataGridSale.Name = "dataGridSale";
            this.dataGridSale.Size = new System.Drawing.Size(523, 420);
            this.dataGridSale.TabIndex = 19;
            this.dataGridSale.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridSale_CellClick);
            // 
            // SaleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 465);
            this.Controls.Add(this.dataGridSale);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimeSale);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numProductCount);
            this.Controls.Add(this.comboContactId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboProductId);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SaleForm";
            this.Text = "Վաճառք";
            this.Load += new System.EventHandler(this.SaleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numProductCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboProductId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboContactId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numProductCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimeSale;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridSale;
    }
}