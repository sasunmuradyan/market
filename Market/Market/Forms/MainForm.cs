﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Market
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ProductInsert_Click(object sender, EventArgs e)
        {
            ProductForm productsForm = new ProductForm("Insert");
            productsForm.Show();
        }
        private void ProductDelete_Click(object sender, EventArgs e)
        {
            ProductForm productsForm = new ProductForm("Delete");
            productsForm.Show();
        }

        private void ProductUpdate_Click(object sender, EventArgs e)
        {
            ProductForm productsForm = new ProductForm("Update");
            productsForm.Show();
        }
        private void PGroupInsert_Click(object sender, EventArgs e)
        {
            PGroupForm pGroupsForm = new PGroupForm("Insert");
            pGroupsForm.Show();
        }

        private void PGroupDelete_Click(object sender, EventArgs e)
        {
            PGroupForm pGroupsForm = new PGroupForm("Delete");
            pGroupsForm.Show();
        }

        private void PGroupUpdate_Click(object sender, EventArgs e)
        {
            PGroupForm pGroupsForm = new PGroupForm("Update");
            pGroupsForm.Show();
        }

        private void ContactInsert_Click(object sender, EventArgs e)
        {
            ContactForm contactForm = new ContactForm("Insert");
            contactForm.Show();
        }

        private void ContactDelete_Click(object sender, EventArgs e)
        {
            ContactForm contactForm = new ContactForm("Delete");
            contactForm.Show();
        }

        private void ContactUpdate_Click(object sender, EventArgs e)
        {
            ContactForm contactForm = new ContactForm("Update");
            contactForm.Show();
        }

        private void SalesInsert_Click(object sender, EventArgs e)
        {
            SaleForm saleForm = new SaleForm("Insert");
            saleForm.Show();
        }

        private void SalesDelete_Click(object sender, EventArgs e)
        {
            SaleForm saleForm = new SaleForm("Delete");
            saleForm.Show();
        }

        private void SalesUpdate_Click(object sender, EventArgs e)
        {
            SaleForm saleForm = new SaleForm("Update");
            saleForm.Show();
        }
    }
}
