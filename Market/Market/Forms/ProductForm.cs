﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Market.Classes;

namespace Market
{
    public partial class ProductForm : Form
    {
        Product _product = new Product();
        string _qType = "";
        public ProductForm()
        {
            InitializeComponent();
        }

        public ProductForm(string qType)
        {
            InitializeComponent();
            _qType = qType;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ProductsForm_Load(object sender, EventArgs e)
        {
            PGroup pGroups = new PGroup();
            DataTable dt = pGroups.GetDataTable();
            //comboGroupId.Items.Add(dt.Rows[0].ItemArray[0].ToString());
            comboGroupId.ValueMember = "id";
            comboGroupId.DisplayMember = "group_name";
            comboGroupId.DataSource = dt;
            switch (_qType)
            {
                case "Insert":
                    button1.Text = "Ավելացնել";
                    break;
                case "Update":
                    button1.Text = "Խմբագրել";
                    break;
                case "Delete":
                    button1.Text = "Ջնջել";
                    break;
                default:
                    break;
            }
            dataGridProduct.DataSource = _product.GetDataTable();
        }

        private void dataGridProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((_qType == "Update") || (_qType == "Delete"))
            {
                Product product = new Product();
                product = product.GetProductsById((int)dataGridProduct.Rows[e.RowIndex].Cells[0].Value);
                txtName.Text = product.productName;
                txtDesc.Text = product.description;
                txtPrice.Text = product.price.ToString();
                comboGroupId.SelectedValue = product.pgroupId;
               
                _product = product;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _product.productName = txtName.Text;
            _product.description = txtDesc.Text;
            _product.price = decimal.Parse(txtPrice.Text);
            _product.pgroupId = (int)comboGroupId.SelectedValue;           

            switch (_qType)
            {
                case "Insert":
                    _product.Add();
                    dataGridProduct.DataSource = _product.GetDataTable();
                    break;
                case "Update":
                    _product.Update();
                    dataGridProduct.DataSource = _product.GetDataTable();
                    break;
                case "Delete":
                    _product.DeleteById();
                    dataGridProduct.DataSource = _product.GetDataTable();
                    break;
                default:
                    break;
            }
        }
    }
}
