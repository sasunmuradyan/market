﻿namespace Market
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ProductMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.ContactMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContactInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.ContactDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ContactUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.PGroupMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PGroupInsert = new System.Windows.Forms.ToolStripMenuItem();
            this.PGroupDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.PGroupUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductMenuItem,
            this.ContactMenuItem,
            this.SalesMenuItem,
            this.PGroupMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ProductMenuItem
            // 
            this.ProductMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductInsert,
            this.ProductDelete,
            this.ProductUpdate});
            this.ProductMenuItem.Name = "ProductMenuItem";
            this.ProductMenuItem.Size = new System.Drawing.Size(69, 20);
            this.ProductMenuItem.Text = "Ապրանք";
            // 
            // ProductInsert
            // 
            this.ProductInsert.Name = "ProductInsert";
            this.ProductInsert.Size = new System.Drawing.Size(137, 22);
            this.ProductInsert.Text = "ավելացնել";
            this.ProductInsert.Click += new System.EventHandler(this.ProductInsert_Click);
            // 
            // ProductDelete
            // 
            this.ProductDelete.Name = "ProductDelete";
            this.ProductDelete.Size = new System.Drawing.Size(137, 22);
            this.ProductDelete.Text = "ջնջել";
            this.ProductDelete.Click += new System.EventHandler(this.ProductDelete_Click);
            // 
            // ProductUpdate
            // 
            this.ProductUpdate.Name = "ProductUpdate";
            this.ProductUpdate.Size = new System.Drawing.Size(137, 22);
            this.ProductUpdate.Text = "խմբագրել";
            this.ProductUpdate.Click += new System.EventHandler(this.ProductUpdate_Click);
            // 
            // ContactMenuItem
            // 
            this.ContactMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContactInsert,
            this.ContactDelete,
            this.ContactUpdate});
            this.ContactMenuItem.Name = "ContactMenuItem";
            this.ContactMenuItem.Size = new System.Drawing.Size(78, 20);
            this.ContactMenuItem.Text = "Կոնտակտ";
            // 
            // ContactInsert
            // 
            this.ContactInsert.Name = "ContactInsert";
            this.ContactInsert.Size = new System.Drawing.Size(180, 22);
            this.ContactInsert.Text = "ավելացնել";
            this.ContactInsert.Click += new System.EventHandler(this.ContactInsert_Click);
            // 
            // ContactDelete
            // 
            this.ContactDelete.Name = "ContactDelete";
            this.ContactDelete.Size = new System.Drawing.Size(180, 22);
            this.ContactDelete.Text = "ջնջել";
            this.ContactDelete.Click += new System.EventHandler(this.ContactDelete_Click);
            // 
            // ContactUpdate
            // 
            this.ContactUpdate.Name = "ContactUpdate";
            this.ContactUpdate.Size = new System.Drawing.Size(180, 22);
            this.ContactUpdate.Text = "խմբագրել";
            this.ContactUpdate.Click += new System.EventHandler(this.ContactUpdate_Click);
            // 
            // SalesMenuItem
            // 
            this.SalesMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SalesInsert,
            this.SalesDelete,
            this.SalesUpdate});
            this.SalesMenuItem.Name = "SalesMenuItem";
            this.SalesMenuItem.Size = new System.Drawing.Size(67, 20);
            this.SalesMenuItem.Text = "Վաճառք";
            // 
            // SalesInsert
            // 
            this.SalesInsert.Name = "SalesInsert";
            this.SalesInsert.Size = new System.Drawing.Size(180, 22);
            this.SalesInsert.Text = "ավելացնել";
            this.SalesInsert.Click += new System.EventHandler(this.SalesInsert_Click);
            // 
            // SalesDelete
            // 
            this.SalesDelete.Name = "SalesDelete";
            this.SalesDelete.Size = new System.Drawing.Size(180, 22);
            this.SalesDelete.Text = "ջնջել";
            this.SalesDelete.Click += new System.EventHandler(this.SalesDelete_Click);
            // 
            // SalesUpdate
            // 
            this.SalesUpdate.Name = "SalesUpdate";
            this.SalesUpdate.Size = new System.Drawing.Size(180, 22);
            this.SalesUpdate.Text = "խմբագրել";
            this.SalesUpdate.Click += new System.EventHandler(this.SalesUpdate_Click);
            // 
            // PGroupMenuItem
            // 
            this.PGroupMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PGroupInsert,
            this.PGroupDelete,
            this.PGroupUpdate});
            this.PGroupMenuItem.Name = "PGroupMenuItem";
            this.PGroupMenuItem.Size = new System.Drawing.Size(114, 20);
            this.PGroupMenuItem.Text = "Ապրանքի Խումբ";
            // 
            // PGroupInsert
            // 
            this.PGroupInsert.Name = "PGroupInsert";
            this.PGroupInsert.Size = new System.Drawing.Size(137, 22);
            this.PGroupInsert.Text = "ավելացնել";
            this.PGroupInsert.Click += new System.EventHandler(this.PGroupInsert_Click);
            // 
            // PGroupDelete
            // 
            this.PGroupDelete.Name = "PGroupDelete";
            this.PGroupDelete.Size = new System.Drawing.Size(137, 22);
            this.PGroupDelete.Text = "ջնջել";
            this.PGroupDelete.Click += new System.EventHandler(this.PGroupDelete_Click);
            // 
            // PGroupUpdate
            // 
            this.PGroupUpdate.Name = "PGroupUpdate";
            this.PGroupUpdate.Size = new System.Drawing.Size(137, 22);
            this.PGroupUpdate.Text = "խմբագրել";
            this.PGroupUpdate.Click += new System.EventHandler(this.PGroupUpdate_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Market";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ProductMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductInsert;
        private System.Windows.Forms.ToolStripMenuItem ProductDelete;
        private System.Windows.Forms.ToolStripMenuItem ProductUpdate;
        private System.Windows.Forms.ToolStripMenuItem ContactMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ContactInsert;
        private System.Windows.Forms.ToolStripMenuItem ContactDelete;
        private System.Windows.Forms.ToolStripMenuItem ContactUpdate;
        private System.Windows.Forms.ToolStripMenuItem SalesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesInsert;
        private System.Windows.Forms.ToolStripMenuItem SalesDelete;
        private System.Windows.Forms.ToolStripMenuItem SalesUpdate;
        private System.Windows.Forms.ToolStripMenuItem PGroupMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PGroupInsert;
        private System.Windows.Forms.ToolStripMenuItem PGroupDelete;
        private System.Windows.Forms.ToolStripMenuItem PGroupUpdate;
    }
}

