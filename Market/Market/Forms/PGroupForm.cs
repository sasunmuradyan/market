﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Market.Classes;

namespace Market
{
    public partial class PGroupForm : Form
    {
        PGroup _pGroups = new PGroup();
        string _qType = "";
        public PGroupForm()
        {
            InitializeComponent();
        }

        public PGroupForm(string qType)
        {
            InitializeComponent();
            _qType = qType;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            _pGroups.groupName = txtName.Text;
            _pGroups.description = txtDesc.Text;          

            switch (_qType)
            {
                case "Insert":    
                    _pGroups.Add();
                    dataGridGroup.DataSource = _pGroups.GetDataTable();
                    break;
                case "Update":
                    _pGroups.Update();
                    dataGridGroup.DataSource = _pGroups.GetDataTable();
                    break;
                case "Delete":
                    _pGroups.DeleteById();
                    dataGridGroup.DataSource = _pGroups.GetDataTable();
                    break;
                default:
                    break;
            }

        }

        private void PGroupsForm_Load(object sender, EventArgs e)
        {
            switch (_qType)
            {
                case "Insert":
                    button1.Text = "Ավելացնել";
                    break;
                case "Update":
                    button1.Text = "Խմբագրել";
                    break;
                case "Delete":
                    button1.Text = "Ջնջել";
                    break;
                default:
                    break;
            }
            dataGridGroup.DataSource = _pGroups.GetDataTable();
        }

        private void dataGridGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridGroup_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((_qType == "Update") || (_qType == "Delete"))
            {
                PGroup pGroups = new PGroup();
                pGroups = pGroups.GetPGroupsById((int)dataGridGroup.Rows[e.RowIndex].Cells[0].Value);
                txtName.Text = pGroups.groupName;
                txtDesc.Text = pGroups.description;
                _pGroups = pGroups;
            }
        }
    }
}
