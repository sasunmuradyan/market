﻿using Market.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Market
{
    public partial class ContactForm : Form
    {
        Contact _contact = new Contact();
        string _qType = "";
        public ContactForm()
        {
            InitializeComponent();
        }
        public ContactForm(string qType)
        {
            InitializeComponent();
            _qType = qType;
        }

        private void ContactForm_Load(object sender, EventArgs e)
        {
            switch (_qType)
            {
                case "Insert":
                    button1.Text = "Ավելացնել";
                    break;
                case "Update":
                    button1.Text = "Խմբագրել";
                    break;
                case "Delete":
                    button1.Text = "Ջնջել";
                    break;
                default:
                    break;
            }
            dataGridContact.DataSource = _contact.GetDataTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _contact.firstName = txtFirstName.Text;
            _contact.lastName = txtLastName.Text;
            _contact.phone = txtPhone.Text;
            _contact.email = txtEmail.Text;

            switch (_qType)
            {
                case "Insert":
                    _contact.Add();
                    dataGridContact.DataSource = _contact.GetDataTable();
                    break;
                case "Update":
                    _contact.Update();
                    dataGridContact.DataSource = _contact.GetDataTable();
                    break;
                case "Delete":
                    _contact.DeleteById();
                    dataGridContact.DataSource = _contact.GetDataTable();
                    break;
                default:
                    break;
            }
        }

        private void dataGridContact_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((_qType == "Update") || (_qType == "Delete"))
            {
                Contact contact = new Contact();
                contact = contact.GetContactsById((int)dataGridContact.Rows[e.RowIndex].Cells[0].Value);
                txtFirstName.Text = contact.firstName;
                txtLastName.Text = contact.lastName;
                txtPhone.Text = contact.phone;
                txtEmail.Text = contact.email;
                _contact = contact;
            }
        }
    }
}
